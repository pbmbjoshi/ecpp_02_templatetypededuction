#include <iostream>
using namespace std;

/*
template<typename T>
void f(ParamType param);
f(expr); 
*/


/* Case 1: ParamType is a Reference */
template<typename T>
void f_case1(T& param) // param is a reference
{
    cout << "address of param : " << &param << endl;
    T temp = param;
    cout << "address of temp : " << &temp << endl;
    //temp = temp+temp; /*NOTE 1, T is const int */
    //param = param+param;  /*NOTE 1*/
}

/* Case 1A: ParamType is a const Reference */
template<typename T>
void f_case1A(const T& param) // param is a reference
{
    cout << "address of param : " << &param << endl;
    T temp = param;
    cout << "address of temp : " << &temp << endl;
    temp = temp+temp; 
    //param = param+param;  /*NOTE 1*/
}

/* Case 2: ParamType is a universal Reference */
template<typename T>
void f_case2(T&& param) // param is a reference
{
    cout << "address of param : " << &param << endl;
    T temp = param;
    cout << "address of temp : " << &temp << endl;
    //temp = temp+temp; 
    // param = param+param;  /*NOTE 1*/ /* NOTE 2 */
}

/* Case 3: ParamType is Neither a Pointer nor a Reference */
template<typename T>
void f_case3(T param) // param is a reference
{
    cout << "address of param : " << &param << endl;
    T temp = param;
    cout << "address of temp : " << &temp << endl;
    temp = temp+temp; 
    param = param+param;  /*NOTE 1*/ /* NOTE 2 */
}

/* Case 3B: ParamType is Neither a Pointer nor a Reference, but is of const */
template<typename T>
void f_case3B(const T param) // param is a reference
{
    cout << "address of param : " << &param << endl;
    T temp = param;
    cout << "address of temp : " << &temp << endl;
    temp = temp+temp; 
    //param = param+param;  /*NOTE 1*/ /* NOTE 2 */
}


int main()
{

    /* Case 1*/
    int x = 27;
    const int cx = x;
    const int& rx = x;
    cout << "address x : " << &x << endl;
    cout << "address cx : " << &cx << endl;
    cout << "address rx : " << &rx << endl;
        
    f_case1(x);
    f_case1(cx); /*NOTE 1: uncommenting  statement in f_case1, will lead to compilation error*/
    f_case1(rx); /*NOTE 1: uncommenting  statement in f_case1, will lead to compilation error*/
    //f_case1(27);
    
    f_case1A(x); /*NOTE 1: uncommenting  statement in f_case1, will lead to compilation error*/
    f_case1A(cx); /*NOTE 1: uncommenting  statement in f_case1, will lead to compilation error*/
    f_case1A(rx); /*NOTE 1: uncommenting  statement in f_case1, will lead to compilation error*/
    f_case1A(27);
    
    f_case2(x); // NOTE 2 : uncommenting this will work
    f_case2(cx); /*NOTE 1, NOTE 2 */
    f_case2(rx); /*NOTE 1, NOTE 2 */
    f_case2(27);
    
    f_case3(x);
    f_case3(cx);
    f_case3(rx);
    f_case3(27);


    
    f_case3B(x);
    f_case3B(cx); /*NOTE 1, NOTE 2 */
    f_case3B(rx); /*NOTE 1, NOTE 2 */
    f_case3B(27);  /*NOTE 1, NOTE 2 */

    
    //cout << x << endl;
    return 0;
}
